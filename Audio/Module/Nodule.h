//
// Copyright (c) 2020-2021 LucKey Productions.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#pragma once

#include "../Scene/Serializable.h"
#include "../Math/Polynomial.h"
#include "ModuleDefs.h"

namespace Dry
{

class Module;
/// %Synthesizer nodule.
class DRY_API Nodule : public Serializable
{
    DRY_OBJECT(Nodule, Serializable);

public:
    static unsigned lastNoduleId_;

    /// Construct.
    explicit Nodule(Context* context);
    /// Destructor.
    ~Nodule() override;
    /// Register object factory.
    static void RegisterObject(Context* context);
    /// Assign to a module and clear connections when needed. Called by module.
    void SetModule(Module* module);
    /// Return the result for a given t, assuming only one output.
    virtual float Solve(float t, unsigned output = 0u);
    /// Check nodule for providing constant output.
    virtual bool IsConstant() { return type_ == NT_NONE; }
    /// Set the input value of a port directly.
    void SetInputValue(unsigned port, float value);

    /// Connect a nodule.
    virtual bool Connect(unsigned input, Nodule* nodule, unsigned output = 0);

//    /// Set nodule type.
//    void SetNoduleType(NoduleType type);
    /// Set nodule type to splitter.
    void SetSplitter();
    /// Set nodule type to multiplier.
    void SetMultiplier();
    /// Set nodule type to gain. A multiplier with two inputs.
    void SetGain();
    /// Set nodule type to clamper.
    void SetClamper();
    /// Set nodule type to oscillator.
    void SetOscillator(unsigned coefficients = 2u);
    /// Set nodule type to stepper.
    void SetStepper();
    /// Set nodule type to life.
    void SetLife();
    /// Returns the nodule's type.
    NoduleType GetNoduleType() const { return type_; }

    /// Set the oscillator's wave type.
    void SetWaveType(WaveType type) { waveType_ = type; }
    /// Set the wave type and default input values based on a Polynomial.
    void SetPolynomial(const Polynomial& polynomial, bool maintainForm = true);

    /// Returns the nodule id.
    unsigned GetId() const { return id_; }
    /// Returns the module the nodule belongs to.
    Module* GetModule() const { return module_; }
    /// Return the number of intputs.
    unsigned GetNumInputs() const { return inputs_.Size(); }
    /// Return the number of outputs.
    unsigned GetNumOutputs() const { return lastResults_.Size(); }
    /// Returns the oscillator wavetype.
    WaveType GetWaveType() const
    {
        if (type_ != NT_OSCILLATOR)
            return WT_NONE;
        else
            return waveType_;
    }

    /// Returns one of the nodule's inputs.
    NoduleInput& GetInput(unsigned index) { return inputs_.At(index); }
    /// Return one of the nodule's inputs, const.
    const NoduleInput& GetInput(unsigned index) const { return inputs_.At(index); }
    /// Returns the current value of an input.
    float GetInputValue(unsigned index) const
    {
        if (index >= inputs_.Size())
            return 0.f;

        return inputs_.At(index).value_;
    }
    /// Returns the last value of an output.
    float GetOutputValue(unsigned index) const
    {
        if (index >= lastResults_.Size())
            return 0.f;

        return lastResults_.At(index);
    }
    /// Returns wether any inputs are connected to other nodules.
    bool HasInput() const
    {
        for (unsigned i{ 0u }; i < GetNumInputs(); ++i)
            if (inputs_.At(i).sender_ != M_MAX_UNSIGNED)
                return true;

        return false;
    }

    /// Load from JSON data. Return true if successful.
    bool LoadJSON(const JSONValue& source) override;
    /// Save to JSON data. Return true if successful.
    bool SaveJSON(JSONValue& dest) const override;

protected:
    /// Set the number of inputs.
    void SetNumInputs(unsigned inputs);
    /// Set the number of outputs.
    virtual void SetNumOutputs(unsigned outputs);
    /// Set the number of inputs and outputs in one call.
    void SetNumPorts(unsigned inputs, unsigned outputs);
    /// Traverse down connections and update.
    void UpdateInputs(float t);
    /// Set the value of an output.
    void SetOutputValue(unsigned port, float value);
    /// Solve custom nodule types, for overloading.
    virtual float SolveUnknown(float t, unsigned output) { return 0.f; }

    /// Upstream connections.
    Vector<NoduleInput> inputs_;
    /// Nodule type.
    NoduleType type_;
    /// Last calculated value for each output.
    PODVector<float> lastResults_;
    /// T value of last calculation. Prevents needless calculations and short circuits.
    float lastT_;

private:
    /// Set nodule ID. Called by constructor.
    void SetId()
    {
        id_ = lastNoduleId_++;

        if (lastNoduleId_ == M_MAX_UNSIGNED)
            lastNoduleId_ = 0u;
    }

    /// Multiply a range of input values.
    float SolveMultiplier() const;
    /// Clamp input between two values.
    float SolveClamper() const;
    /// Harmonic oscillator.
    float SolveOscillator(float t) const;
    /// FMod one input based on another.
    float SolveStepper() const;
    /// 8x8 Game of life to float value.
    float SolveLife(float t);

    /// Nodule ID.
    unsigned id_;

    /// Parent module.
    Module* module_;
    /// The oscillator's wave type.
    WaveType waveType_;
};

}

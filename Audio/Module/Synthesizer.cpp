//
// Copyright (c) 2020-2021 LucKey Productions.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../Core/Context.h"
#include "../Core/CoreEvents.h"
#include "../Scene/Scene.h"
#include "../Scene/SceneEvents.h"
#include "Dry/Audio/Audio.h"
#include "Dry/Audio/SoundListener.h"
#include "Dry/IO/Log.h"

#include "Synthesizer.h"


namespace Dry
{

extern const char* DRY_AUDIO_CATEGORY;

Synthesizer::Synthesizer(Context* context): Component(context),
    stream_{ nullptr },
    module_{ nullptr },
    elapsedTime_{ 0.f },
    sceneTime_{ true },
    dopplerEffect_{ false },
    speedOfSound_{ 342.f },
    updateEnabled_{ true }
{
    stream_ = new BufferedSoundStream();
    stream_->SetFormat(44100, true, false);
}

void Synthesizer::RegisterObject(Context* context)
{
    context->RegisterFactory<Synthesizer>(DRY_AUDIO_CATEGORY);
}

void Synthesizer::OnSceneSet(Scene* scene)
{
    if (scene)
        SubscribeToEvent(E_UPDATE, DRY_HANDLER(Synthesizer, HandleAttributeAnimationUpdate));
    else
        UnsubscribeFromEvent(E_UPDATE);
}

void Synthesizer::HandleAttributeAnimationUpdate(StringHash eventType, VariantMap& eventData)
{
    if (!updateEnabled_ || !module_)
        return;

    Update(/*eventData[AttributeAnimationUpdate::P_TIMESTEP].GetFloat()*/);
}

void Synthesizer::Update(/*float timeStep*/)
{
    const float t0{        (sceneTime_ ? GetScene()->GetElapsedTime()
                                       : GetSubsystem<Time>()->GetElapsedTime()) };
    const float timeScale{ (sceneTime_ ? GetScene()->GetTimeScale()
                                       : 1.f) };


    float doppler{ DopplerFactor() };


    // Try to keep 1/10 seconds of sound in the buffer, to avoid both dropouts and unnecessary latency
    const float targetLength{ 1.f/10.f };
    const float requiredLength{ targetLength - stream_->GetBufferLength() };
    const float streamFrequency{ stream_->GetFrequency() };

    if (requiredLength < 0.f)
        return;

    unsigned numSamples{ static_cast<unsigned>(streamFrequency * requiredLength) };

    if (!numSamples)
        return;

    // Allocate a new buffer and fill it. The sound is over-amplified (distorted),
    // clamped to the 16-bit range, and finally lowpass-filtered according to the coefficient
    SharedArrayPtr<signed short> newData{ new signed short[numSamples] };

    for (unsigned i{ 0u }; i < numSamples; ++i)
    {
        const float t{ elapsedTime_ };
        const unsigned port{ 0u };

        newData[i] = static_cast<int>(Clamp(module_->Solve(t, port)
                                            * 32767.0f, -32767.0f, 32767.0f));

        elapsedTime_ += timeScale * doppler / streamFrequency;
    }

    stream_->AddData(newData, numSamples * sizeof(signed short));
}

float Synthesizer::DopplerFactor()
{
    if (!dopplerEffect_)
    {
        return 1.f;
    }
    else
    {
        SoundListener* listener{ GetSubsystem<Audio>()->GetListener() };
        if (listener)
        {
            Node* listenerNode{ listener->GetNode() };

            // Determine relative velocity and divide over speed of sound.
            if (listenerNode)
            {
                const float velocity{ 0.f }; // <- insert velocity here
                return  (speedOfSound_ + velocity) / speedOfSound_;
            }
        }
    }

    return 1.f;
}


Module* Synthesizer::CreateModule(unsigned inputs)
{
    module_ = context_->CreateObject<Module>();
    module_->Setup(stream_->IsStereo(), inputs);

    return module_;
}


}

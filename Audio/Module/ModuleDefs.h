//
// Copyright (c) 2020-2021 LucKey Productions.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#pragma once

#include "Dry/Math/MathDefs.h"

namespace Dry
{

/// An upstream nodule connection.
struct NoduleInput
{
    /// Construct with defaults.
    NoduleInput() :
        sender_{ M_MAX_UNSIGNED },
        port_{ M_MAX_UNSIGNED },
        value_{ 0.f }
    {
    }
    /// Connect to a nodule.
    void Connect(unsigned noduleId, unsigned port)
    {
        sender_ = noduleId;
        port_   = port;
    }
    /// Disconnect.
    void Disconnect(bool reset = false)
    {
        sender_ = M_MAX_UNSIGNED;
        port_   = M_MAX_UNSIGNED;

        if (reset)
            value_ = 0.f;
    }

    /// Sender nodule ID.
    unsigned sender_;
    /// Sender nodule port.
    unsigned port_;
    /// Lasting calculated result.
    float value_;
};

/// Nodule type.
enum NoduleType
{
    NT_NONE = 0,
    NT_SPLITTER,
    NT_MULTIPLIER,
    NT_GAIN,
    NT_CLAMPER,
    NT_OSCILLATOR,
    NT_STEPPER,
    NT_LIFE,
    NT_MODULE,
    NT_UNKNOWN
};

enum WaveType
{
    WT_NONE = 0,
    WT_POLYNOMIAL,
    WT_SINE,     WT_COSINE,
    WT_TRIANGLE, WT_COTRIANGLE,
    WT_PULSE,    WT_COPULSE,
    WT_SAW,      WT_COSAW/*,
    WT_GAUSSIAN ? */
};

}

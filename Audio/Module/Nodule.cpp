//
// Copyright (c) 2020-2021 LucKey Productions.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../Resource/JSONFile.h"
#include "../Core/Context.h"
#include "../IO/Log.h"

#include "Module.h"

namespace Dry
{

const char* noduleTypeNames[] =
{
    "None",
    "Splitter",
    "Multiplier",
    "Gain",
    "Clamper",
    "Oscillator",
    "Stepper",
    "Life",
    "Module",
    "Unknown",
    nullptr
};

const char* waveTypeNames[] =
{
    "None",
    "Polynomial",
    "Sine",
    "Cosine",
    "Triangle",
    "Cotriangle",
    "Pulse",
    "Copulse",
    "Saw",
    "Cosaw",
    nullptr
};

unsigned Nodule::lastNoduleId_{ 0u };

Nodule::Nodule(Dry::Context* context): Serializable(context),
    inputs_{},
    type_{ NT_NONE },
    lastResults_{ { 0.f } },
    lastT_{ M_INFINITY },
    id_{ M_MAX_UNSIGNED },
    module_{ nullptr },
    waveType_{ WT_SINE }
{
    SetId();
}

Nodule::~Nodule() = default;

void Nodule::RegisterObject(Context* context)
{
    context->RegisterFactory<Nodule>();
}

bool Nodule::LoadJSON(const JSONValue& source)
{

    return true;
}

bool Nodule::SaveJSON(JSONValue& dest) const
{
    // Write type and ID
    dest.Set("type", noduleTypeNames[GetNoduleType()]);

    if (type_ == NT_OSCILLATOR)
        dest.Set("wavetype", waveTypeNames[GetWaveType()]);

    dest.Set("id", id_);

    if (type_ != NT_MODULE)
        dest.Set("outputs", GetNumOutputs());

    JSONArray inputElems{};

    if (GetNumInputs())
    {
        for (NoduleInput input : inputs_)
        {
            JSONValue inputElem{};

            inputElem.Set("value",  input.value_);

            if (input.sender_ != M_MAX_UNSIGNED)
                inputElem.Set("connection", JSONArray{ input.sender_, input.port_ });

            inputElems.Push(inputElem);
        }

        dest.Set("inputs", inputElems);
    }


    return true;
}

void Nodule::SetModule(Module* module)
{
    if (module != module_)
    {
        for (NoduleInput input: inputs_)
            input.Disconnect();
    }

    module_ = module;
}

float Nodule::Solve(float t, unsigned output)
{
    if (output >= lastResults_.Size())
        return 0.f;
    else if (lastT_ == t || IsConstant())
        return lastResults_.At(output);

    lastT_ = t;

    float res{};

    // Traverse dependency nodules.
    UpdateInputs(t);

    // Solve single output nodules.
    switch (type_)
    {
    default: break;
    case NT_GAIN:
    case NT_MULTIPLIER: res = SolveMultiplier(); break;
    case NT_CLAMPER:    res = SolveClamper();    break;
    case NT_OSCILLATOR: res = SolveOscillator(t); break;
    case NT_STEPPER:    res = SolveStepper();    break;
    case NT_LIFE:       res = SolveLife(t);       break;
    case NT_UNKNOWN:    res = SolveUnknown(t, output); break;
    }

    if (type_ == NT_SPLITTER)
        return lastResults_.At(output);

    lastResults_.At(0) = res;
    return res;
}

void Nodule::SetInputValue(unsigned port, float value)
{
    if (port >= inputs_.Size())
        return;

    inputs_.At(port).value_ = value;
}

bool Nodule::Connect(unsigned input, Nodule* nodule, unsigned output)
{
    if (output >= nodule->GetNumOutputs())
            return false;

    if ((type_ == NT_MULTIPLIER || type_ == NT_LIFE)
        && input >= GetNumInputs())
    {
        SetNumInputs(input + 1u);
    }

    NoduleInput& port{ inputs_.At(input) };
    port.Connect(nodule->GetId(), output);

    return true;
}

//void Nodule::SetNoduleType(NoduleType type)
//{
//    switch (type) {
//    case NT_SPLITTER:   SetSplitter();   break;
//    case NT_MULTIPLIER: SetMultiplier(); break;
//    case NT_GAIN:       SetGain();       break;
//    case NT_CLAMPER:    SetClamper();    break;
//    case NT_OSCILLATOR: SetOscillator(); break;
//    case NT_STEPPER:    SetStepper();    break;
//    default: break;
//    }
//}

void Nodule::SetSplitter()
{
    type_ = NT_SPLITTER;

    SetNumPorts(1u, 1u);
}

void Nodule::SetMultiplier()
{
    type_ = NT_MULTIPLIER;

    SetNumOutputs(1u);
}

void Nodule::SetGain()
{
    type_ = NT_GAIN;

    SetNumPorts(2u, 1u);

    SetInputValue(0u, 1.f);
    SetInputValue(1u, 1.f);
}

void Nodule::SetClamper()
{
    type_ = NT_CLAMPER;

    SetNumPorts(3u, 1u);

    SetInputValue(1, -1.f);
    SetInputValue(2,  1.f);
}

void Nodule::SetOscillator(unsigned coefficients)
{
    type_ = NT_OSCILLATOR;

    SetNumPorts(coefficients + 2u, 1u);

    SetInputValue(1, 1.f);
    SetInputValue(3, 1.f);
}


void Nodule::SetPolynomial(const Polynomial& polynomial, bool maintainForm)
{
    const PODVector<float> coefficients{ polynomial.GetCoefficients() };
    const Vector2 slope{ polynomial.GetSlope() };
    const PolynomialType polynomialType{ polynomial.GetPolynomialType() };

    SetOscillator(coefficients.Size());

    SetInputValue(0u, slope.x_);
    SetInputValue(1u, slope.y_);

    for (unsigned c{ 0u }; c < coefficients.Size(); ++c)
    {
        SetInputValue(c + 2u, coefficients.At(c));
    }

    if (maintainForm)
    {
        switch (waveType_)
        {
        default:
        case WT_SINE: case WT_COSINE:
        {
            waveType_ = polynomialType == PT_HARMONIC_COS ? WT_COSINE : WT_SINE;
        }
        break;
        case WT_TRIANGLE: case WT_COTRIANGLE:
        {
            waveType_ = polynomialType == PT_HARMONIC_COS ? WT_COTRIANGLE : WT_TRIANGLE;
        }
        break;
        case WT_SAW: case WT_COSAW:
        {
            waveType_ = polynomialType == PT_HARMONIC_COS ? WT_COSAW : WT_SAW;
        }
        break;
        case WT_PULSE: case WT_COPULSE:
        {
            waveType_ = polynomialType == PT_HARMONIC_COS ? WT_COPULSE : WT_PULSE;
        }
        break;
        case WT_POLYNOMIAL: break;
        }
    }
    else
    {
        switch (polynomialType)
        {
        default:
        case PT_HARMONIC_SIN: waveType_ = WT_SINE;       break;
        case PT_HARMONIC_COS: waveType_ = WT_COSINE;     break;
        case PT_POLYNOMIAL:   waveType_ = WT_POLYNOMIAL; break;
        }
    }
}

void Nodule::SetStepper()
{
    type_ = NT_STEPPER;

    SetNumPorts(2u, 1u);

    SetInputValue(1, 1.f/4.f);
}

void Nodule::SetLife()
{
    type_ = NT_LIFE;

    SetNumOutputs(1u);
}

void Nodule::SetNumInputs(unsigned inputs)
{
    inputs_.Resize(inputs, {});
}

void Nodule::SetNumOutputs(unsigned outputs)
{
    lastResults_.Resize(outputs, 0.f);
}

void Nodule::SetNumPorts(unsigned inputs, unsigned outputs)
{
    SetNumInputs(inputs);
    SetNumOutputs(outputs);
}

void Nodule::UpdateInputs(float t)
{
    if (!module_)
        return;

    for (unsigned i{ 0 }; i < inputs_.Size(); ++i)
    {
        NoduleInput& input{ inputs_.At(i) };

        if (input.sender_ != M_MAX_UNSIGNED)
        {
            Nodule* sender{ module_->GetNodule(input.sender_) } ;
            if (sender)
                input.value_ = sender->Solve(t, input.port_);
        }

        if (type_ == NT_SPLITTER)
        {
            lastResults_.At(i) = inputs_.At(i).value_;
        }
    }
}

void Nodule::SetOutputValue(unsigned port, float value)
{
    if (port >= lastResults_.Size())
        return;

    lastResults_.At(port) = value;
}

float Nodule::SolveMultiplier() const
{
    if (GetNumInputs() == 0u)
        return 0.f;

    float res{ GetInputValue(0u) };
    for (unsigned i{ 1u }; i < GetNumInputs(); ++i)
    {
        if (inputs_.At(i).sender_ != M_MAX_UNSIGNED || type_ == NT_GAIN)
            res *= GetInputValue(i);
    }

    return res;
}

float Nodule::SolveClamper() const
{
    return Clamp(GetInputValue(0u),
                 GetInputValue(1u),
                 GetInputValue(2u));
}

float Nodule::SolveOscillator(float t) const
{
    Polynomial polynomial{};
    PODVector<float> coefficients{};
    PolynomialType polynomialType{ PT_HARMONIC_SIN };

    if (waveType_ == WT_POLYNOMIAL)
        polynomialType = PT_POLYNOMIAL;
    else if (waveType_ == WT_COSINE)
        polynomialType = PT_HARMONIC_COS;

    polynomial.SetSlope({ GetInputValue(0u), GetInputValue(1u) });

    for (unsigned i{ 2 }; i < inputs_.Size(); ++i)
    {
        coefficients.Push(GetInputValue(i));
    }

    polynomial.SetCoefficients(coefficients);
    polynomial.SetPolynomialType(polynomialType);

    return polynomial.Solve(polynomialType != PT_POLYNOMIAL ? (fmodf(t, GetInputValue(1u) == 0.f ? 0.f : 1 / GetInputValue(1u)))
                                                            : t);
}

float Nodule::SolveStepper() const
{
    const float stepSize{ GetInputValue(1u) };

    if (stepSize == 0.f)
        return GetInputValue(0u);

    return Floor(GetInputValue(0u)/stepSize) * stepSize;
}

float Nodule::SolveLife(float t)
{
    // Conway's game of life to float, inputs as "rain"?
    return 0.f;
}


}

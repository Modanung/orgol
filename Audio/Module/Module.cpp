//
// Copyright (c) 2020-2021 LucKey Productions.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#include "../Resource/JSONFile.h"
#include "Dry/Core/Context.h"
#include "Dry/IO/Log.h"

#include "Module.h"

namespace Dry
{

Module::Module(Dry::Context* context): Nodule(context),
    nodules_{},
    sockets_{}
{
    type_ = NT_MODULE;
}

Module::~Module()
{
    nodules_.Clear();
}

void Module::RegisterObject(Context* context)
{
    context->RegisterFactory<Module>();
}

bool Module::LoadJSON(const JSONValue& source)
{


    return true;
}

bool Module::SaveJSON(JSONValue& dest) const
{
    // Write type and ID
    Nodule::SaveJSON(dest);

    if (GetNumOutputs() > 0)
    {
        JSONArray socketElems{};

        for (NoduleInput socket: sockets_)
        {
            JSONValue socketElem{};

            if (socket.sender_ != M_MAX_UNSIGNED)
                socketElem.Set("connection", JSONArray{ socket.sender_, socket.port_ });

            socketElem.Set("value",  socket.value_);
            socketElems.Push(socketElem);
        }

        dest.Set("sockets", socketElems);
    }

    JSONArray noduleElems{};

    for (Nodule* n: nodules_.Values())
    {
        JSONValue noduleElem{};
        n->SaveJSON(noduleElem);
        noduleElems.Push(noduleElem);
    }

    dest.Set("nodules", noduleElems);

    return true;
}

void Module::Setup(unsigned inputs, unsigned outputs)
{
    Nodule::SetNumPorts(inputs, outputs);
}

void Module::Setup(bool stereo, unsigned inputs)
{
    Setup(inputs, stereo ? 2u : 1u);
}

float Module::Solve(float t, unsigned output)
{
//    Log::Write(LOG_DEBUG, String{lastResults_.At(0)});


    if (output >= GetNumOutputs())
        return 0.f;
    else if (lastT_ == t || IsConstant())
        return lastResults_.At(output);

    lastT_ = t;

    float res{};

    UpdateInputs(t);
    UpdateSocket(output, t);

    return GetOutputValue(output);
}

void Module::UpdateSocket(unsigned output, float t)
{


    NoduleInput& socket{ sockets_.At(output) };

    if (socket.sender_ != M_MAX_UNSIGNED)
    {
        Nodule* sender{ GetNodule(socket.sender_) } ;

        if (sender)
            socket.value_ = sender->Solve(t, socket.port_);
    }

    SetOutputValue(socket.port_, socket.value_);
}


Nodule* Module::CreateSplitter()
{
    Nodule* splitterNodule{ CreateNodule(NT_SPLITTER) };

    splitterNodule->SetSplitter();

    return splitterNodule;
}

Nodule* Module::CreateMultiplier()
{
    Nodule* multiplierNodule{ CreateNodule(NT_MULTIPLIER) };

    multiplierNodule->SetMultiplier();

    return multiplierNodule;
}

Nodule* Module::CreateGain()
{
    Nodule* gainNodule{ CreateNodule(NT_GAIN) };

    gainNodule->SetGain();

    return gainNodule;
}

Nodule* Module::CreateClamper()
{
    Nodule* clamperNodule{ CreateNodule(NT_CLAMPER) };

    clamperNodule->SetClamper();

    return clamperNodule;
}

Nodule* Module::CreateStepper()
{
    Nodule* stepperNodule{ CreateNodule(NT_STEPPER) };

    stepperNodule->SetStepper();

    return stepperNodule;
}

Nodule* Module::CreateLife()
{
    Nodule* lifeNodule{ CreateNodule(NT_LIFE) };

    lifeNodule->SetLife();

    return lifeNodule;
}

Nodule* Module::CreateOscillator(unsigned harmonics)
{
    Nodule* oscillatorNodule{ CreateNodule(NT_OSCILLATOR) };

    oscillatorNodule->SetOscillator(harmonics);

    return oscillatorNodule;
}

Module* Module::CreateModule(unsigned inPorts, unsigned outPorts)
{
    SharedPtr<Module> module{ context_->CreateObject<Module>() };

    module->SetNumPorts(inPorts, outPorts);
    module->SetModule(this);

    nodules_.Insert({ module->GetId(), module });

    return module;
}

bool Module::Connect(unsigned portM, Nodule* nodule, unsigned portN)
{
    if (nodule == nullptr)
        return false;

    if (nodule->GetModule() == this)
    {

        if (portM >= sockets_.Size() || portN >= nodule->GetNumOutputs())
            return false;

        NoduleInput& socket{ sockets_.At(portM) };
        socket.Connect(nodule->GetId(), portN);

        return true;
    }
    else
    {
        return Nodule::Connect(portM, nodule, portN);
    }

    return false;
}

Nodule* Module::GetNodule(unsigned id) const
{
    if (nodules_.Contains(id))
        return nodules_[id]->Get();
    else
        return nullptr;
}

void Module::SetNumOutputs(unsigned outputs)
{
    Nodule::SetNumOutputs(outputs);

    sockets_.Resize(outputs, {});
}

Nodule* Module::CreateNodule(NoduleType type)
{
    if (type == NT_NONE)
        return nullptr;

    SharedPtr<Nodule> nodule{ context_->CreateObject<Nodule>() };

    nodule->SetModule(this);
    nodules_.Insert({ nodule->GetId(), nodule });
    return nodule;
}

bool Module::RemoveNodule(Nodule* nodule)
{
    const unsigned id{ nodule->GetId() };

    if (id == M_MAX_UNSIGNED)
        return false;

    nodule = GetNodule(id);

    if (nodule)
    {
        // Remove all relevant connections.
        for (Nodule* n: nodules_.Values())
        {
            for (unsigned i{ 0u }; i < n->GetNumInputs(); ++i)
            {
                NoduleInput& input{ n->GetInput(i) };

                if (input.sender_ == id)
                    input.Disconnect();
            }
        }

        nodules_.Erase(id);

        return true;
    }
    else
    {
        return false;
    }
}

}

//
// Copyright (c) 2020-2021 LucKey Productions.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#pragma once

#include "Nodule.h"

namespace Dry
{

/// %Synthesizer module. Handles creation and removal of nodules and connections.
class DRY_API Module : public Nodule
{
    DRY_OBJECT(Module, Nodule);

public:
    /// Construct.
    explicit Module(Context* context);
    /// Destruct, including child nodules.
    ~Module() override;
    /// Register object factory.
    static void RegisterObject(Context* context);
    /// Set the number of inputs and ouptuts of the module.
    void Setup(unsigned inputs, unsigned outputs);
    /// Set mono or stereo output and the number of inputs.
    void Setup(bool stereo, unsigned inputs = 0);
    /// Return the result for a given t.
    float Solve(float t, unsigned output = 0u) override;
    /// Creates and returns a splitter nodule.
    Nodule* CreateSplitter();
    /// Creates and returns a multiplier nodule.
    Nodule* CreateMultiplier();
    /// Creates and returns a multiplier nodule.
    Nodule* CreateGain();
    /// Creates and returns a clamper nodule.
    Nodule* CreateClamper();
    /// Creates and returns a oscillator nodule.
    Nodule* CreateOscillator(unsigned harmonics);
    /// Creates and returns a stepper nodule.
    Nodule* CreateStepper();
    /// Creates and returns a life nodule.
    Nodule* CreateLife();
    /// Creates and returns a module.
    Module* CreateModule(unsigned inPorts, unsigned outPorts);

    /// Connect a nodule.
    bool Connect(unsigned portM, Nodule* noduleId, unsigned portN = 0) override;
    /// Returns a nodule by ID, or a nullptr if it does not exist.
    Nodule* GetNodule(unsigned id) const;
    /// Removes a nodule, returns true on success.
    bool RemoveNodule(Nodule* nodule);

    /// Returns one of the module's sockets.
    NoduleInput& GetSocket(unsigned index) { return sockets_.At(index); }
    /// Return one of the module's sockets, const.
    const NoduleInput& GetSocket(unsigned index) const { return sockets_.At(index); }

    /// Load from JSON data. Return true if successful.
    bool LoadJSON(const JSONValue& source) override;
    /// Save to JSON data. Return true if successful.
    bool SaveJSON(JSONValue& dest) const override;

protected:
    /// Set the number of outputs/sockets.
    void SetNumOutputs(unsigned outputs) override;

private:
    /// Update the module's output values.
    void UpdateSocket(unsigned output, float t);
    /// Creates a new nodule.
    Nodule* CreateNodule(NoduleType type);

    /// Child nodules by id.
    HashMap<unsigned, SharedPtr<Nodule> > nodules_;
    /// The module's out sockets that, internally, behave like inputs.
    Vector<NoduleInput> sockets_;
};


}

//
// Copyright (c) 2020-2021 LucKey Productions.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

#pragma once

#include "Module.h"
#include "Dry/Audio/BufferedSoundStream.h"

#include "Dry/Scene/Component.h"

namespace Dry
{

/// %Synthesizer that convert nodule's output to an audio stream.
class DRY_API Synthesizer : public Component
{
    DRY_OBJECT(Synthesizer, Component);

public:
    /// Construct.
    Synthesizer(Context* context);
    /// Register object factory.
    static void RegisterObject(Context* context);

    /// Returns the synthesizers's sound stream.
    BufferedSoundStream* GetStream() { return stream_; }
    /// Creates and returns a new root nodule.
    Module* CreateModule(unsigned inputs = 0u);
    /// Set the synthesizer module.
    void SetModule(Module* module) { module_ = module; }
    /// Returns the synthesizer module.
    Module* GetModule() const { return module_; }

protected:
    /// Handle scene being assigned.
    void OnSceneSet(Scene* scene) override;
    /// Handle the attribute animation update event sent by scene.
    void HandleAttributeAnimationUpdate(StringHash eventType, VariantMap& eventData);

private:
    /// Update the stream based on module output.
    void Update();
    /// Returns the Doppler effect factor.
    float DopplerFactor();

    /// The synthesizer's sound stream.
    SharedPtr<BufferedSoundStream> stream_;
    /// The root nodule.
    SharedPtr<Module> module_;
    /// Elapsed time. Increased with each sample.
    float elapsedTime_;
    /// Use scene time scale.
    bool sceneTime_;
    /// Enables Doppler effect.
    bool dopplerEffect_;
    /// Speed of sound in meters per second, to scale the Doppler effect.
    float speedOfSound_;
    /// Update enabled flag.
    bool updateEnabled_;
};


/// %Component that transforms its node based on nodule output.
class DRY_API Transformer : public Component
{
//    DRY_OBJECT(Transformer, Component);

//public:
//    /// Construct.
//    Transformer(Context* context);
};


}

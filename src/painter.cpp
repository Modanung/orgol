/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Dry/2D/Drawable2D.h"
#include "Dry/Graphics/GraphicsEvents.h"

#include "painter.h"

Painter::Painter(Context* context): Object(context),
    scene_{ new Scene{ context } },
    camera_{   scene_->CreateChild("Camera")->CreateComponent<Camera>() },
    renderer_{ scene_->CreateComponent<DebugRenderer>() }
{
    renderer_->SetLineAntiAlias(true);
    scene_->CreateComponent<Octree>();

    SharedPtr<Viewport> viewport{ new Viewport{ context_, scene_, camera_ } };
    SharedPtr<RenderPath> renderPath{ viewport->GetRenderPath()->Clone() };
    viewport->SetRenderPath(renderPath);

    RENDERER->SetViewport(0, viewport);

    Node* cameraNode{ camera_->GetNode() };
    cameraNode->LookAt(Vector3::BACK, Vector3::DOWN);
    cameraNode->SetWorldPosition(Vector3{GRAPHICS->GetWidth() / 2.f, GRAPHICS->GetHeight() / 2.f, 0.f });
    camera_->SetOrthographic(true);
    camera_->SetOrthoSize(GRAPHICS->GetHeight());

    SubscribeToEvent(E_SCREENMODE, DRY_HANDLER(Painter, HandleScreenModeChanged));
}

void Painter::HandleScreenModeChanged(StringHash eventType, VariantMap &eventData)
{
    camera_->SetOrthoSize(GRAPHICS->GetHeight());
}

void Painter::DrawLine(const Vector2& from, const Vector2& to, const Style& style)
{
    renderer_->AddLine( { from }, { to },
                        style.strokeColor_, false);
}

void Painter::DrawPolynomial(const TypedPolynomial<Vector2>& polynomial, const Style& style, int segments, float start, float end)
{
    const float dt{ 1.f / segments };

    for (int i{ 0 }; i < segments; ++i)
    {
        const Vector2 from{ polynomial.Solve((i) * dt) };
        const Vector2 to{ polynomial.Solve((i + 1.f) * dt) };

        renderer_->AddLine(from, to, style.strokeColor_, false);
    }
}

void Painter::DrawEllipse(const Vector2& center, const Vector2& size, const Style& style, int segments)
{
    DrawPolynomial(Ellipse(center, size), style, segments);
}

TypedPolynomial<Vector2> Painter::Line(const Vector2& from, const Vector2& to)
{
    TypedPolynomial<Vector2> line{};
    line.SetCoefficient(0, from);
    line.SetCoefficient(0, to - from);

    return line;
}

TypedPolynomial<Vector2> Painter::Ellipse(const Vector2& center, const Vector2& size, float deform)
{
    TypedPolynomial<Vector2> ellipse{};
    ellipse.SetPolynomialType(0, PT_HARMONIC_SIN);
    ellipse.SetPolynomialType(1, PT_HARMONIC_COS);
    ellipse.SetCoefficient(0, center);
    ellipse.SetCoefficient(1, size * .5f);

    ellipse.SetCoefficient(5,   size * deform / 16.f);
    ellipse.SetCoefficient(9,  -size * deform / 32.f);
    ellipse.SetCoefficient(13,  size * deform / 64.f);
    ellipse.SetCoefficient(17, -size * deform / 128.f);

    return ellipse;
}

TypedPolynomial<Vector2> Painter::Hypocycloid(const Vector2& center, const Vector2& size, int cusps, Vector2 pointiness)
{
    if (size.x_ == 0.f)
        return {};

    const float ratio{ size.y_ / size.x_ };
    const float a{ .5f * size.x_ };
    const float b{ a / cusps };

    TypedPolynomial<Vector2> hypocycloid{};
    hypocycloid.SetPolynomialType(0, PT_HARMONIC_SIN);
    hypocycloid.SetPolynomialType(1, PT_HARMONIC_COS);
    hypocycloid.SetCoefficient(0, center);
    hypocycloid.SetCoefficient(1,         Vector2{ 1.f, ratio } * (a - b));
    hypocycloid.SetCoefficient(cusps - 1, Vector2{ -1.f * pointiness.y_, ratio * pointiness.x_} * b);

    return hypocycloid;
}

/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "Dry/Resource/JSONFile.h"

#include "modulemaster.h"

ModuleMaster::ModuleMaster(Context* context): Object(context),
    openModules_{},
    activeModule_{ nullptr }
{
    SubscribeToEvent(E_KEYDOWN, DRY_HANDLER(ModuleMaster, HandleKeyDown));
}

void ModuleMaster::HandleKeyDown(StringHash eventType, VariantMap& eventData)
{
    const unsigned key{ eventData[KeyDown::P_KEY].GetUInt() };
    const unsigned qualifiers{ eventData[KeyDown::P_QUALIFIERS].GetUInt() };

    if ((qualifiers & QUAL_CTRL) != QUAL_NONE)
    {
        if (key == KEY_N)
            NewModule();
        else if (key == KEY_O)
            {}//Open
        else if (key == KEY_S)
            SaveActiveModule();
    }
}

Module* ModuleMaster::NewModule(unsigned inputs, unsigned outputs)
{

    SharedPtr<Module> module{ context_->CreateObject<Module>() };
    module->Setup(inputs, outputs);
    openModules_.Push(module);
    SetActiveModule(module);

    return activeModule_;
}

bool ModuleMaster::SaveActiveModule()
{
    String fileName{ FILES->GetCurrentDir() + "Test.json"};

    SharedPtr<JSONFile> json{ context_->CreateObject<JSONFile>() };
    JSONValue& root{ json->GetRoot() };

    Log::Write(LOG_INFO, String{ activeModule_->SaveJSON(root) ? "Save successful" : "Save failed" });

    File file{context_, fileName, FILE_WRITE};
    return json->Save(file, "    ");
}

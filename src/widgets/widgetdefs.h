/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef WIDGETDEFS_H
#define WIDGETDEFS_H

#include "../../Audio/Module/ModuleDefs.h"

static const int NODULE_PORTSIZE{ 7 };

enum PortType{ PORT_IN = 0, PORT_OUT, PORT_UNDEFINED, PORT_ALL };

static const HashMap<unsigned, float> deformation
{
    { NT_NONE,        0.f },
    { NT_SPLITTER,    0.f },
    { NT_MULTIPLIER, -1.5f },
    { NT_GAIN,         .55f },
    { NT_CLAMPER,     -.666f },
    { NT_OSCILLATOR, -1.f },
    { NT_STEPPER,    -1.f },
    { NT_LIFE,        1.f },
    { NT_MODULE,      0.f }
};

static const HashMap<unsigned, IntVector2> defaultSize
{
    { NT_NONE,       { 1, 1 } },
    { NT_SPLITTER,   { 1, 1 } },
    { NT_MULTIPLIER, { 1, 2 } },
    { NT_GAIN,       { 3, 1 } },
    { NT_CLAMPER,    { 2, 2 } },
    { NT_OSCILLATOR, { 3, 2 } },
    { NT_STEPPER,    { 1, 3 } },
    { NT_LIFE,       { 1, 1 } },
    { NT_MODULE,     { 3, 3 } }
};

static const HashMap<unsigned, Vector2> drawShrink
{
    { NT_NONE,       { 0.f, 0.f } },
    { NT_SPLITTER,   { 4.f, 4.f } },
    { NT_MULTIPLIER, { 8.f, 8.f } },
    { NT_GAIN,       { 13.f, 1.5f } },
    { NT_CLAMPER,    { -1.5f, 10.f } },
    { NT_OSCILLATOR, { 1.f, 6.f } },
    { NT_STEPPER,    { 0.f, 0.f } },
    { NT_LIFE,       { 2.f, 3.f } },
    { NT_MODULE,     { 0.f, 0.f } }
};

#endif // WIDGETDEFS_H

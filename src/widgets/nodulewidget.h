/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef NODULEWIDGET_H
#define NODULEWIDGET_H

#include "Audio/Module/Module.h"

#include "widget.h"


class ModuleEditor;

struct Port
{
    Port(PortType type, unsigned index):
        type_{ type }, index_{ index }
    {
    }

    PortType type_;
    unsigned index_;
};

class NoduleWidget: public Widget
{
public:
    Nodule* GetNodule() const { return nodule_; }
    void SetNodule(Nodule* nodule) { nodule_ = nodule; }

    void Draw(Painter* p) const override;

    void SetCoords(const IntVector2& coords);
    void SetSizeBlocks(const IntVector2& size);
    void SetSelected(bool selected) override;

    static HashSet<IntVector2> GetOccupy(const IntVector2& coords, const IntVector2& size);

    HashSet<IntVector2> GetOccupy(const IntVector2& coords) const { return GetOccupy(coords, sizeBlocks_); }
    HashSet<IntVector2> GetOccupied() { return GetOccupy(coords_); }

    IntVector2 PortCoords(Port port) const;
    virtual IntVector2 PortCoords(PortType type, unsigned index) const;
    virtual PODVector<IntVector2> GetInputCoords() const;
    virtual PODVector<IntVector2> GetOutputCoords() const;
    virtual Port PortAt(const IntVector2& coords) const;

    IntVector2 PortPosition(Port port) const;
    virtual PODVector<IntVector2> GetInputPositions() const;
    virtual PODVector<IntVector2> GetOutputPositions() const;

    IntVector2 CoordsToPosition(const IntVector2& coords) const;
    IntVector2 PositionToCoords() const;

    ModuleEditor* Editor() const;
    void Remove();

    static TypedPolynomial<Vector2> Shape(NoduleType type, const IntVector2& position, const Vector2& size, int outset = 0)
    {
        return Painter::Ellipse(Vector2{ position },
                                size + IntVector2::ONE * outset,
                                *deformation[type]);
    }

    TypedPolynomial<Vector2> Shape(int outset = 0) const
    {
        return Shape(nodule_->GetNoduleType(), GetPosition(), GetDrawSize(), outset);
    }

protected:
    NoduleWidget(Nodule* nodule);

    IntVector2 PortPosition(const IntVector2& coords) const;
    virtual void DrawPorts(Painter* p) const;

    Nodule* nodule_;
    IntVector2 coords_;
    IntVector2 sizeBlocks_;
    float outputPeak_;

    int GridSize() const;

    float deformation_;
    virtual Vector2 GetDrawSize() const
    {
        Vector2 shrink{};
        NoduleType type{ nodule_->GetNoduleType()};

        if (drawShrink.Contains(type))
            shrink = *drawShrink[type];

        return GetSize() - shrink;
    }

private:
    bool sideA_{ true };
    int rotation_{ 0};
};

#endif // NODULEWIDGET_H

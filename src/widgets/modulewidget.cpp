/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../moduleeditor.h"

#include "modulewidget.h"

ModuleWidget::ModuleWidget(Module* module): NoduleWidget(module)
{
    allowDrag_ = false;

    SetSizeBlocks(*defaultSize[NT_MODULE]);
    SetCoords({ 7, 1 });
}

void ModuleWidget::Draw(Painter* p) const
{
    p->DrawPolynomial(p->Hypocycloid(Vector2{ GetPosition() },
                                     Vector2{ GetSize() }, 4),
                      Color::GREEN, 64);
    p->DrawEllipse(Vector2{ GetPosition() },
                   Vector2{ GetSize() },
                   Color::CHARTREUSE FADE);

    DrawPorts(p);

    if (selected_)
        p->DrawEllipse(Vector2{ GetPosition() },
                       Vector2{ GetSize() + IntVector2::ONE * 8 },
                       COL_SELECTED);
}

void ModuleWidget::DrawPorts(Painter* p) const
{
    if (IsActiveModule())
    {
        DrawInnerPorts(p);
    }
    else
    {
        NoduleWidget::DrawPorts(p);
    }
}

void ModuleWidget::DrawInnerPorts(Painter* p) const
{
    // Draw inner inputs and outer outputs
    PODVector<IntVector2> positions{ GetInputPositions() };

    for (const IntVector2& pos: positions)
        p->DrawEllipse(Vector2{ pos }, Vector2{ IntVector2::ONE * NODULE_PORTSIZE },
                       PortColor(PORT_IN) FADE);

    for (const IntVector2& pos: positions)
        p->DrawEllipse(Vector2{ pos } + Vector2{ 0.f, -1.f } * GridSize() * 2.5f, Vector2{ IntVector2::ONE * GridSize() },
                       PortColor(PORT_OUT) FADE, 4);
//    // Draw outputs
//    for (const IntVector2& pos: GetOutputPositions())
//        p->DrawEllipse(Vector2{ pos }, Vector2{ IntVector2::ONE * NODULE_PORTSIZE },
//                       PortColor(PORT_OUT) FADE);

    // Draw sockets

//    PODVector<IntVector2> coords{ GetInputCoords() };
//    const float g{ GridSize() };

//    for (unsigned s{ 0 }; s < coords.Size(); ++s)
//        p->DrawEllipse(Vector2{ Editor()->CoordsToPosition(coords.At(s)) - IntVector2::UP * .5f * g },
    //        { g, g }, Color::YELLOW.Transparent(.7f), 4);
}

IntVector2 ModuleWidget::PortCoords(PortType type, unsigned index) const
{
    if (IsActiveModule())
    {
        if (type == PORT_IN)
        {
            if (index >= nodule_->GetNumOutputs())
                return IntVector2::ONE * M_MAX_INT;

            return GetInputCoords().At(index);
        }
        else if (type == PORT_OUT)
        {
            if (index >= nodule_->GetNumInputs())
                return IntVector2::ONE * M_MAX_INT;

            return GetOutputCoords().At(index);
        }
        else
        {
            return coords_;
        }
    }
    else
    {
        return NoduleWidget::PortCoords(type, index);
    }
}

PODVector<IntVector2> ModuleWidget::GetInputCoords() const
{
    Module* module{ GetModule() };

    if (!module)
        return {};

    if (!IsActiveModule())
        return NoduleWidget::GetInputCoords();

    PODVector<IntVector2> positions{};
    const unsigned numOutputs{ nodule_->GetNumOutputs() };

    for (unsigned i{ 0 }; i < numOutputs; ++i)
    {
        const int w{ sizeBlocks_.x_ - 1 };
        const int h{ sizeBlocks_.y_ - 1 };
        const int index{ static_cast<int>(i) };
        const bool stereo{ numOutputs == 2u };

        const int xShift{ -(w + w % 2) / 2 + !stereo };

        positions.Push(coords_ +
                       IntVector2{ index + xShift, h - (h + h % 2) / 2 });
    }

    return positions;
}

PODVector<IntVector2> ModuleWidget::GetOutputCoords() const
{
    Module* module{ GetModule() };

    if (!module)
        return {};

    if (!IsActiveModule())
        return NoduleWidget::GetOutputCoords();

    PODVector<IntVector2> coords{};
//    const int numInputs{ static_cast<int>(coords.Size()) };

//    for (int c{ 0 }; c < numInputs; ++c)
//    {
//        bool stereo{ coords.Size() == 2u };
//        const IntVector2 shift{ (stereo ? (c % 2) * 2 : (numInputs % 2)), 0 };

//        coords.At(c) += shift;
//    }

    return coords;
}

PODVector<IntVector2> ModuleWidget::GetInputPositions() const
{
    if (!nodule_)
        return {};

    if (!IsActiveModule())
        return NoduleWidget::GetInputPositions();

    PODVector<IntVector2> positions{};
    for (unsigned i{ 0 }; i < nodule_->GetNumOutputs(); ++i)
    {
        positions.Push(PortPosition(PortCoords(PORT_IN, i)));
    }

    return positions;
}

PODVector<IntVector2> ModuleWidget::GetOutputPositions() const
{
    if (!nodule_)
        return {};

    if (!IsActiveModule())
        return NoduleWidget::GetOutputPositions();

    PODVector<IntVector2> positions{};
    for (unsigned o{ 0 }; o < nodule_->GetNumInputs(); ++o)
        positions.Push(PortPosition(PortCoords(PORT_OUT, o)));

    return positions;
}

bool ModuleWidget::IsActiveModule() const
{
    return Editor()->GetActiveModule() == static_cast<Module*>(nodule_);
}

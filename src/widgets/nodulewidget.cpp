/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../moduleeditor.h"

#include "nodulewidget.h"

NoduleWidget::NoduleWidget(Nodule* nodule): Widget(),
    nodule_{ nodule },
    coords_{},
    sizeBlocks_{},
    outputPeak_{}
{
    allowDrag_ = true;

    SetSizeBlocks(*defaultSize[NT_NONE]);
}

void NoduleWidget::Draw(Painter* p) const
{
    if (GetSize().x_ <= 0 || GetSize().y_ <= 0)
        return;

    const int segments{ Painter::SizeToSegments(GetDrawSize()) };


    if (dragged_)
    {
        auto shape{ Shape(6) };
        shape.SetCoefficient(0, Vector2{ CoordsToPosition(PositionToCoords()) });
        Color color{ COL_CURSOR };

        if (!Editor()->CheckFree(GetOccupy(PositionToCoords())))
            color = COL_INVALID;

        p->DrawPolynomial(shape, color, segments);
    }
    else if (selected_)
    {
        p->DrawPolynomial(Shape(8), COL_SELECTED, segments);
    }

    p->DrawPolynomial(Shape(), Color::CHARTREUSE FADE, segments);

    DrawPorts(p);
}



void NoduleWidget::DrawPorts(Painter* p) const
{
    // Draw inputs
    for (const IntVector2& pos: GetInputPositions())
        p->DrawEllipse(Vector2{ pos }, Vector2{ IntVector2::ONE * NODULE_PORTSIZE },
                         PortColor(PORT_IN) FADE);
    // Draw outputs
    for (const IntVector2& pos: GetOutputPositions())
        p->DrawEllipse(Vector2{ pos }, Vector2{ IntVector2::ONE * NODULE_PORTSIZE },
                         PortColor(PORT_OUT) FADE);
}

ModuleEditor* NoduleWidget::Editor() const
{
    return ModuleEditor::Get();
}

int NoduleWidget::GridSize() const
{
    return Editor()->GetGridSize();
}

IntVector2 NoduleWidget::CoordsToPosition(const IntVector2& coords) const
{
    return  Editor()->CoordsToPosition(coords, sizeBlocks_);
}

IntVector2 NoduleWidget::PositionToCoords() const
{
    return Editor()->PositionToCoords(GetPosition(), sizeBlocks_);
}

void NoduleWidget::Remove()
{
    nodule_->GetModule()->RemoveNodule(nodule_);
}


void NoduleWidget::SetCoords(const IntVector2& coords)
{
    coords_ = coords;

    SetPosition(CoordsToPosition(coords_));
}

void NoduleWidget::SetSizeBlocks(const IntVector2& size)
{
    SetSize(GridSize() * size);

    const IntVector2 sizeChange{ size - sizeBlocks_ };
    sizeBlocks_ = size;

    SetCoords(coords_ - sizeChange);
}

void NoduleWidget::SetSelected(bool selected)
{
    Widget::SetSelected(selected);

    if (selected)
        Editor()->SetActiveNodule(nodule_);
    else if (!Editor()->GetSelection().Size())
        Editor()->SetActiveNodule(nullptr);
}

HashSet<IntVector2> NoduleWidget::GetOccupy(const IntVector2& coords, const IntVector2& size)
{
    HashSet<IntVector2> cells{};

    for (int x{ 0 }; x < size.x_; ++x)
    for (int y{ 0 }; y < size.y_; ++y)
    {
        const int width{ size.x_ };
        const int height{ size.y_ };
        cells.Insert(coords + IntVector2{ x - width/2,
                                          y - height/2});
    }

    return cells;
}

IntVector2 NoduleWidget::PortCoords(Port port) const
{
    return PortCoords(port.type_, port.index_);
}

IntVector2 NoduleWidget::PortCoords(PortType type, unsigned index) const
{
    if (type == PORT_IN)
    {
        if (index >= nodule_->GetNumInputs())
            return IntVector2::ONE * M_MAX_INT;

        return GetInputCoords().At(index);
    }
    else if (type == PORT_OUT)
    {
        if (index >= nodule_->GetNumOutputs())
            return IntVector2::ONE * M_MAX_INT;

        return GetOutputCoords().At(index);
    }
    else
    {
        return coords_;
    }
}

PODVector<IntVector2> NoduleWidget::GetInputCoords() const
{
    PODVector<IntVector2> positions{};

    if (!nodule_)
        return positions;

    for (unsigned i{ 0 }; i < nodule_->GetNumInputs(); ++i)
    {
        const int w{ sizeBlocks_.x_ - 1 };
        const int h{ sizeBlocks_.y_ - 1 };
        const int index{ static_cast<int>(i) };
        const int xShift{ -(w + w % 2) / 2 };

        positions.Push(coords_ +
                       IntVector2{ index + xShift, h - (h + h % 2) / 2 });
    }

    return positions;
}

PODVector<IntVector2> NoduleWidget::GetOutputCoords() const
{
    PODVector<IntVector2> positions{};

    if (!nodule_)
        return positions;

    const int outputs{ static_cast<int>(nodule_->GetNumOutputs()) };
    for (int o{ 0 }; o < outputs; ++o)
    {
        const int w{ sizeBlocks_.x_ - 1 };
        const int h{ sizeBlocks_.y_ - 1 };

        const int xShift{ -(w + w % 2) / 2 };

        positions.Push(coords_ +
                       IntVector2{ o + xShift, -(h + h % 2) / 2 });
    }

    return positions;
}

Port NoduleWidget::PortAt(const IntVector2& coords) const
{
    Port port{ PORT_ALL, M_MAX_UNSIGNED };

    if (!nodule_ || IsDragged())
        return port;

    PODVector<IntVector2> outputs{ GetOutputCoords() };
    for (unsigned o{ 0 }; o < outputs.Size(); ++o)
    {
        if (outputs.At(o) == coords)
            return { PORT_OUT, o };
    }

    PODVector<IntVector2> inputs{ GetInputCoords() };
    for (unsigned i{ 0 }; i < inputs.Size(); ++i)
    {
        if (inputs.At(i) == coords)
            return { PORT_IN, i };
    }
    return port;
}

PODVector<IntVector2> NoduleWidget::GetInputPositions() const
{
    PODVector<IntVector2> positions{};

    if (!nodule_)
        return positions;

    for (unsigned i{ 0 }; i < nodule_->GetNumInputs(); ++i)
    {
        positions.Push(PortPosition(PortCoords(PORT_IN, i)));
    }

    return positions;
}

PODVector<IntVector2> NoduleWidget::GetOutputPositions() const
{
    if (!nodule_)
        return {};

    PODVector<IntVector2> positions{};
    for (unsigned o{ 0 }; o < nodule_->GetNumOutputs(); ++o)
        positions.Push(PortPosition(PortCoords(PORT_OUT, o)));

    return positions;
}

IntVector2 NoduleWidget::PortPosition(Port port) const
{
    return PortPosition(PortCoords(port));
}

IntVector2 NoduleWidget::PortPosition(const IntVector2& coords) const
{
    const IntVector2 shift{ (VectorMod(sizeBlocks_ + IntVector2::ONE, { 2, 2 })) };

    return  (GetPosition() + shift * GridSize()/2) + (coords - coords_) * GridSize();
}

/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../moduleeditor.h"

#include "connectorwidget.h"

//ConnectorWidget::ConnectorWidget(Connector* connector): NoduleWidget(connector->sender_)
//{
//}

void Link::Draw(Painter* p) const
{
    for (int i{ 0 }; i < 2 /*4*/; ++i)
    {
        const Vector2 posA{ GetPositionA() };
        const Vector2 posB{ GetPositionB() };
        const Vector2 direction{ (posB - posA).Normalized() };
        Color color{ (IsValid() ? Color::GREEN : Color::GRAY) };

        for (int i{ 0 }; i < 2; ++i)
        {
            Vector2 right{ direction.Rotated(90.f) * NODULE_PORTSIZE * (.5f - i) };
            p->DrawLine(right + GetPositionA() + direction * NODULE_PORTSIZE,
                        right + GetPositionB() - direction * NODULE_PORTSIZE, color.Transparent(.7f));
        }
    }
}

NoduleWidget* Link::GetWidgetA() const
{
    return static_cast<NoduleWidget*>(GUI::Get()->GetWidgetById(GetWidgetIdA()));
}

NoduleWidget* Link::GetWidgetB() const
{
    return static_cast<NoduleWidget*>(GUI::Get()->GetWidgetById(GetWidgetIdB()));
}

Vector2 Link::GetPositionA() const
{
    GUI* gui{ GUI::Get() };
    NoduleWidget* nw{ static_cast<NoduleWidget*>(gui->GetWidgetById(GetWidgetIdA())) };

    if (nw)
        return Vector2{ nw->PortPosition(GetPortA()) };
    else
        return Vector2::ONE * M_INFINITY;
}

Vector2 Link::GetPositionB() const
{
    GUI* gui{ GUI::Get() };
    NoduleWidget* nw{ static_cast<NoduleWidget*>(gui->GetWidgetById(GetWidgetIdB())) };

    if (nw)
        return Vector2{ nw->PortPosition(GetPortB()) };
    else
        return Vector2::ONE * M_INFINITY;
}

bool Link::IsValid() const
{
    if (GetPortTypeA() == PORT_OUT && GetPortTypeB() == PORT_IN)
        return true;
    else
        return false;
}

bool Link::Validate()
{
    if (GetPortTypeA() == PORT_OUT && GetPortTypeB() == PORT_IN)
    {
        return true;
    }
    else if (GetPortTypeA() == PORT_IN && GetPortTypeB() == PORT_OUT)
    {
        Reverse();
        return true;
    }
    else
    {
        return false;
    }
}


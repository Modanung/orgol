/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#ifndef CONNECTOR_H
#define CONNECTOR_H

#include "nodulewidget.h"

struct Link: private Pair<Pair<unsigned, Port>, Pair<unsigned, Port>>
{
    Link(NoduleWidget* A, const Port& portA, NoduleWidget* B, const Port& portB):
        Pair<Pair<unsigned int, Port>, Pair<unsigned int, Port> >(
        { A->GetId(), portA },
        { B->GetId(), portB })
    {
    }

    void Draw(Painter* p) const;

    unsigned GetWidgetIdA() const { return Link::first_ .first_; }
    unsigned GetWidgetIdB() const { return Link::second_.first_; }

    NoduleWidget* GetWidgetA() const;
    NoduleWidget* GetWidgetB() const;

    Nodule* GetNoduleA() const { return GetWidgetA()->GetNodule(); }
    Nodule* GetNoduleB() const { return GetWidgetB()->GetNodule(); }

    Port GetPortA() const { return Link::first_ .second_; }
    Port GetPortB() const { return Link::second_.second_; }

    PortType GetPortTypeA() const { return GetPortA().type_; }
    PortType GetPortTypeB() const { return GetPortB().type_; }

    Vector2 GetPositionA() const;
    Vector2 GetPositionB() const;

    bool IsValid() const;
    bool Validate();
    void Reverse() { Swap(Link::first_, Link::second_); }
};

//class ConnectorWidget: public NoduleWidget
//{
//public:
//    ConnectorWidget(Connector* connector);
//};

#endif // CONNECTOR_H

/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef SPLITTERWIDGET_H
#define SPLITTERWIDGET_H

//#include "../module/splitter.h"

#include "nodulewidget.h"

struct Unconnection: private Pair<unsigned, unsigned>
{
    Unconnection(unsigned A, unsigned B): Pair<unsigned, unsigned>(A, B)
    {
    }

    bool operator ==(const Unconnection& rhs)
    {
        if ((A() == rhs.A() && B() == rhs.B())
         || (A() == rhs.B() && B() == rhs.A()))
            return true;
        else
            return false;
    }

    unsigned A() const { return Unconnection::first_; }
    unsigned B() const { return Unconnection::second_; }
};

class SplitterWidget: public NoduleWidget
{
public:
    static void AddUnconnection(unsigned A, unsigned B)
    {
        Unconnection newUnconnection{ A, B };
        if (!UnconnectionExists(newUnconnection))
            unconnections_.Push(newUnconnection);
    }
    static bool UnconnectionExists(const Unconnection& unconnection)
    {
        for (unsigned u{ 0 }; u < unconnections_.Size(); ++u)
            if (unconnections_.At(u) == unconnection)
                return true;

        return false;
    }

    static PODVector<unsigned> Traverse(unsigned splitter)
    {
        PODVector<unsigned> network{ splitter };
        PODVector<unsigned> checked{};
        bool added{ true };

        while (added)
        {
            PODVector<unsigned> unchecked{};
            for (const unsigned n: network)
            {
                if (!checked.Contains(n))
                    unchecked.Push(n);
            }

            if (unchecked.IsEmpty())
            {
                added = false;
            }
            else
            {
                for (const unsigned n: unchecked)
                {
                    for (const Unconnection u: unconnections_)
                    {
                        if (u.A() == splitter && !network.Contains(u.B()))
                            network.Push(u.B());
                        else if (u.B() == splitter && !network.Contains(u.A()))
                            network.Push(u.A());
                    }

                    checked.Push(n);
                }
            }
        }

        return network;
    }

    SplitterWidget(Nodule* splitter);

    bool HasInput() const { return nodule_->HasInput(); }
    void DrawPorts(Painter* p) const override;
    Port PortAt(const IntVector2& coords) const override;

protected:
    static PODVector<Unconnection> unconnections_; // Splitter-to-splitter ids of undefined connections.
};

#endif // SPLITTERWIDGET_H

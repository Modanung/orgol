/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef OSCILLATORWIDGET_H
#define OSCILLATORWIDGET_H

//#include "../module/oscillator.h"

#include "nodulewidget.h"

class OscillatorWidget: public NoduleWidget
{
public:
    OscillatorWidget(Nodule* oscillator);

    PODVector<IntVector2> GetInputCoords() const override;
    PODVector<IntVector2> GetOutputCoords() const override;

    void SetPolynomial(const Polynomial& polynomial);

private:
    void UpdateNumPorts();
};

#endif // OSCILLATORWIDGET_H

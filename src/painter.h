/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef PAINTER_H
#define PAINTER_H

#include "mastercontrol.h"

#define FADE .Transparent(visible_ * (.6f + .2f * (hovered_ + (clicked_ || selected_))))
#define COL_SELECTED Color::ROSE.Transparent(.5f)
#define COL_CURSOR Color::AZURE.Transparent(.55f)
#define COL_INVALID Color::RED.Transparent(.666f)


struct Style
{
    Style(): Style(Color::WHITE)
    {
    }

    Style(const Color& stroke, const Color& fill = Color::TRANSPARENT_BLACK, const PODVector<float> dash = {}):
        strokeColor_{ stroke },
        fillColor_{ fill },
        dash_{ dash }
    {
    }

    Style(const Color& stroke, const PODVector<float> dash): Style(stroke, Color::TRANSPARENT_BLACK, dash)
    {
    }

    Color strokeColor_;
    Color fillColor_;

    PODVector<float> dash_{};
};

class Painter: public Object
{
    DRY_OBJECT(Painter, Object);

public:
    static int SizeToSegments(const Vector2& size) { return (size.x_ + size.y_) * .5f; }
    Painter(Context* context);

    void DrawLine(const Vector2& from, const Vector2& to, const Style& style = {});
    void DrawPolynomial(const TypedPolynomial<Vector2>& polynomial, const Style& style, int segments, float start = 0.f, float end = 1.f);
    void DrawEllipse(const Vector2& center, const Vector2& size, const Style& style = {}, int segments = 64);

    static TypedPolynomial<Vector2> Line(const Vector2& from, const Vector2& to);
    static TypedPolynomial<Vector2> Ellipse(const Vector2& center, const Vector2& size, float deform = 0.f);
    static TypedPolynomial<Vector2> Hypocycloid(const Vector2& center, const Vector2& size, int cusps, Vector2 pointiness = Vector2::ONE);

private:
    void HandleScreenModeChanged(StringHash eventType, VariantMap& eventData);

    Scene* scene_;
    Camera* camera_;
    DebugRenderer* renderer_;
};

#endif // PAINTER_H

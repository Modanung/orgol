/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#pragma once

#include "../Core/Object.h"

namespace Dry
{

DRY_EVENT(E_NODULECREATED, NoduleCreated)
{
    DRY_PARAM(P_NODULE, Nodule); // Nodule pointer
}

DRY_EVENT(E_SELECTIONCHANGED, SelectionChanged)
{
    DRY_PARAM(P_WIDGET, Widget); // Widgetpointer
}

//DRY_EVENT(E_CONNECTSTART, NumPortsChanged)
//{
//    DRY_PARAM(P_NODULE, Nodule); // Nodule pointer
//    DRY_PARAM(P_IN, In);         // Int
//    DRY_PARAM(P_OUT, Out);       // Int
//}

};

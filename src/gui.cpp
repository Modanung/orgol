/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "widgets/widget.h"
#include "moduleeditor.h"

#include "gui.h"

GUI* GUI::instance_{ nullptr };
unsigned GUI::lastId_{ 0u };


GUI::GUI(Context* context): Object(context),
    widgets_{},
    hoverPos_{  M_MAX_INT, M_MAX_INT },
    clickBegin_{ M_MAX_INT, M_MAX_INT }
{
    instance_ = this;

    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(GUI, Draw));
    SubscribeToEvent(E_MOUSEMOVE,        DRY_HANDLER(GUI, HandleMouseMove));
    SubscribeToEvent(E_MOUSEBUTTONDOWN,  DRY_HANDLER(GUI, HandleMouseButtonDown));
    SubscribeToEvent(E_MOUSEBUTTONUP,    DRY_HANDLER(GUI, HandleMouseButtonUp));
}

void GUI::HandleMouseMove(StringHash eventType, VariantMap& eventData)
{
    const IntVector2 mousePos{ eventData[MouseMove::P_X].GetInt(),
                               eventData[MouseMove::P_Y].GetInt() };


    for (Widget* w: GetWidgets())
    {
        if (w->IsHovered())
        {
            hoverPos_ = mousePos;

            if (w->AllowsDrag() && w->IsClicked() && !w->IsDragged())
            {
                const IntRect stay{ clickBegin_ - IntVector2::ONE * 4,
                                    clickBegin_ + IntVector2::ONE * 4 };

                if (!stay.IsInside(hoverPos_))
                {
                    GetSubsystem<ModuleEditor>()->Drag(w);
                    w->SetDragged(true);
                }
            }
        }

        if (w->IsDragged())
        {
            const IntVector2 mouseMove{ eventData[MouseMove::P_DX].GetInt(),
                        eventData[MouseMove::P_DY].GetInt() };

            w->SetPosition(w->GetDragBegin() + mousePos - clickBegin_);
        }

        if (!w->IsDragged() && (!w->IsClicked() || !w->AllowsDrag()))
            w->SetHovered(w->GetRect().IsInside(mousePos));
    }
}

void GUI::HandleMouseButtonDown(StringHash eventType, VariantMap &eventData)
{
    if (GetSubsystem<ModuleEditor>()->IsWiring())
        return;

    for (Widget* w: GetWidgets())
    {
        if (w->IsHovered())
        {
            const bool click{ eventData[MouseButtonDown::P_BUTTON].GetInt() == MOUSEB_LEFT };

            if (click && click != w->IsClicked())
                clickBegin_ = hoverPos_;

            w->SetClicked(click);
        }
    }
}

void GUI::HandleMouseButtonUp(StringHash eventType, VariantMap &eventData)
{
    if (GetSubsystem<ModuleEditor>()->IsWiring())
        return;

    if (eventData[MouseButtonUp::P_BUTTON].GetInt() == MOUSEB_LEFT)
    {
        const unsigned qualifiers{ eventData[MouseButtonUp::P_QUALIFIERS].GetUInt() };
        bool shift{   (qualifiers & QUAL_SHIFT) != QUAL_NONE};
        bool ctrl{ (qualifiers & QUAL_CTRL)  != QUAL_NONE};

        for (Widget* w: GetWidgets())
        {
            if (w->IsDragged())
            {
                GetSubsystem<ModuleEditor>()->Drop(w);
            }
            else if (w->IsClicked() && w->IsHovered())
            {
                GetSubsystem<ModuleEditor>()->Select(w, shift || ctrl);
            }
            else if (!shift && !ctrl)
            {
                w->SetSelected(false);
            }

            w->SetClicked(false);
        }
    }
}

void GUI::Draw(StringHash /*eventType*/, VariantMap &/*eventData*/)
{
    Painter* painter{ GetSubsystem<Painter>() };
    ModuleEditor* editor{ GetSubsystem<ModuleEditor>() };

    editor->Draw(painter);
}

void GUI::AddWidget(Widget* w)
{
    widgets_.Insert({ w->GetId(), w });
}

void GUI::RemoveWidget(Widget* w)
{
    w->SetSelected(false);
    widgets_.Erase(w->GetId());
}

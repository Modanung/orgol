/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef GUI_H
#define GUI_H

#include "painter.h"

class Widget;
class NoduleWidget;

class GUI: public Object
{
    DRY_OBJECT(GUI, Object);

public:
    GUI(Context* context);
    static GUI* Get() { return instance_; };

    static unsigned FreeWidgetId()
    {
        if (lastId_ == M_MAX_UNSIGNED)
            lastId_ = 0u;

        return lastId_++;
    };

    void AddWidget(Widget* w);
    void RemoveWidget(Widget* w);

    Vector<Widget*> GetWidgets() const { return widgets_.Values(); }
    Widget* GetWidgetById(unsigned id) const
    {
        if (widgets_.Contains(id))
            return *widgets_[id];
        else
            return nullptr;
    }

private:
    void Draw(StringHash eventType, VariantMap& eventData);

    void HandleMouseMove(StringHash eventType, VariantMap& eventData);
    void HandleMouseButtonDown(StringHash eventType, VariantMap& eventData);
    void HandleMouseButtonUp(StringHash eventType, VariantMap& eventData);

    HashMap<unsigned, Widget*> widgets_;

    IntVector2 hoverPos_;
    IntVector2 clickBegin_;

    static GUI* instance_;
    static unsigned lastId_;
};

#endif // GUI_H

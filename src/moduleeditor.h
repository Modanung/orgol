/* Dry Orgol
// Copyright (C) 2021 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef MODULEEDITOR_H
#define MODULEEDITOR_H

#include "modulemaster.h"

#include "widgets/nodulewidget.h"
#include "editorevents.h"

#include "widgets/panels/toolbox.h"
#include "widgets/panels/tweaker.h"

#include "Audio/Module/Module.h"

struct Hand
{
    NoduleType type_{ NT_NONE };
    IntVector2 cell_{ IntVector2::ONE * M_MAX_INT };
    bool valid_{ true };
    int rotation_{ 0 };
};

struct Hair
{
    NoduleWidget* noduleWidget_{ nullptr };
    IntVector2 from_{};
    IntVector2 to_{};
    PortType fromPortType_{ PORT_ALL };
    Color color_{ Color::GRAY };
};

class Link;

using NoduleWidgetPort = Pair<NoduleWidget*, Port>;

class ModuleEditor: public Object
{
    DRY_OBJECT(ModuleEditor, Object);

public:
    static ModuleEditor* Get() { return instance_; }
    ModuleEditor(Context* context);

    void Draw(Painter* p) const;
    int GetGridSize() { return grid_; }
    IntVector2 CoordsToPosition(const IntVector2& coords, const IntVector2& size = IntVector2::ONE) const;
    IntVector2 PositionToCoords(const IntVector2 position, const IntVector2 size = IntVector2::ONE) const;
    IntVector2 ClampCoords(const IntVector2 coords, const IntVector2 size) const;

    PODVector<NoduleWidget*> GetNoduleWidgets(Module* module) const;
    PODVector<NoduleWidget*> NoduleWidgetsAt(const IntVector2 coords) const;
    NoduleWidget* NoduleWidgetAt(const IntVector2 coords) const;
    PODVector<NoduleWidget*> GetSelection() const;
    Module* GetActiveModule() const { return GetSubsystem<ModuleMaster>()->GetActiveModule(); }

    void Select(Widget* widget, bool add = false);
    void Drag(Widget* widget);
    void Drop(Widget* widget);
    bool CheckFree(const HashSet<IntVector2>& cells);
    bool IsWiring() const { return wiring_; }
    const IntVector2& GetHoverCell() const { return hoverCell_; }
    const Hair& GetHair() const { return hair_; }
    NoduleWidgetPort NoduleWidgetPortAt(const IntVector2& coords);
    Port PortAt(const IntVector2& coords);
    PortType PortTypeAt(const IntVector2& coords);
    bool PortMatch(PortType typeA, PortType typeB);

    void NewModule();
    void SetActiveNodule(Nodule* nodule) { activeNodule_ = nodule; }

private:
    static ModuleEditor* instance_;

    void CreateTooltip();
    void UpdateTooltip(StringHash eventType, VariantMap& eventData);

    void DrawGrid(Painter* p) const;
    void DrawHand(Painter* p) const;
    void DrawHair(Painter* p) const;
    void DrawHUD(Painter* p) const;

    void HandleMouseButtonDown(StringHash eventType, VariantMap& eventData);
    void HandleMouseMove(StringHash eventType, VariantMap& eventData);
    void HandleMouseWheel(StringHash eventType, VariantMap& eventData);
    void HandleKeyDown(StringHash eventType, VariantMap& eventData);

    void HandleLeftClick();
    void HandleRightClick();
    void HandleValidMouseRight();
    void HandleMiddleClick();
    void Laser();
    void StartWiring();
    void EndWiring();

    void UpdateHand();
    NoduleWidget* PlaceNodule(NoduleType type, const IntVector2 coords);
    void DeleteSelection();
    void DeleteAt(const IntVector2 coords);

    void AddLink(NoduleWidget* noduleWidget);
    void CreateConnection(Pair<Nodule*, unsigned> from, Pair<Nodule*, unsigned> to);

    ToolBox toolBox_;
    Tweaker tweaker_;
    TypedPolynomial<Vector2> scope_;
    Text* tooltip_;
    SharedPtr<Scene> scene_;
    SoundSource* soundSource_;
    Synthesizer* synthesizer_;
    Nodule* activeNodule_;

    int grid_;
    IntVector2 hoverCell_;
    Hand hand_;
    bool wiring_;
    Hair hair_;
    void UpdateHair();

    bool dragging_;
    bool splitterAtHand_;
    bool rightHeld_;

    PODVector<Link> links_;
};

#endif // MODULEEDITOR_H
